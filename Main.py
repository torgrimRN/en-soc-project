# EN-SOC 3001 exam project, 2014. Python code

import time
import socket
from threading import Thread
import serial
from SimpleCV import *
from flask import Flask,send_file,request
import MySQLdb

# Arduino serial port
port = "/dev/ttyACM0"

# setup the serial port and baudrate
SerialIOArduino = serial.Serial(port,9600)

# default sample frequency
frequency = 60 

# Set to 1 when the temperature readout thread starts 
temperatureThreadRunning = 0 

# Set to 1 when the web server thread starts
webServerRunning = 0

# Remove old inputs
SerialIOArduino.flushInput()

# Default IP address of Windows Computer
UDP_IP = "0.0.0.0" 

# Port number
UDP_PORT = 9050

# Internet protocol user datagram (UDP)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 

# Listen on all adapters
sock.bind(("0.0.0.0", UDP_PORT))         


''' Method to initiate and run the web server '''
def runWebServer():
    app = Flask(__name__)
    cam = Camera()
    
    # Return a web camera image to the URL 128.39.113.153:5000/webcam
    @app.route('/webcam')
    def SendImage():
        img = cam.getImage()
        img.save("myImg.jpg")
        return send_file('myImg.jpg', mimetype='image/jpg')

    # Everyone is allowed access the to the server
    if __name__ == '__main__':
        app.run(host='0.0.0.0')
''' End runWebServer '''


''' Send temperature values from Arduino to the Windows Computer
    and log temperatures in a MySQL database '''
def sendTemperature():
    while True:
        if (SerialIOArduino.inWaiting() > 0):
            inputLine = SerialIOArduino.readline().strip()
            sock.sendto(inputLine, (UDP_IP, UDP_PORT))

            # Create a connection to the MySQL database
            connection = MySQLdb.connect("localhost", "OrchidBreeder",
                                     "FlowerPower", "OrchidWatcher")

            # Cursor object to execute SQL commands
            curs=connection.cursor()

            if float(inputLine) < 25.0 and float(inputLine) >= 20.0:
                try:
                    # Insert date, time and temperature into the "normal" table
                    curs.execute ("""INSERT INTO normal 
                    values(CURRENT_DATE(), NOW(), %s)""" %inputLine )

                    # Commit changes
                    connection.commit()
                    print "Data committed to normal"

                except:
                    print "Error: the database is being rolled back"
                    connection.rollback()
            
                connection.close()
                
            else:
                try:
                    curs.execute ("""INSERT INTO deviated 
                    values(CURRENT_DATE(), NOW(), %s)""" %inputLine )
                    connection.commit()
                    print "Data committed to deviations"

                except:
                    print "Error: the database is being rolled back"
                    connection.rollback()
            
                connection.close()
''' End sendTemperature '''


''' Method for controlling servo motor 1 '''
def servo1Direction(direction, angle):

    # Maximum and minimum angles
    maxAngle = 175
    minAngle = 5
    
    if direction == 'LEFT':

        # Change servo angle
        if angle < maxAngle:
            angle += 5

            # Create a command string
            servoString1 = "$SERVO1," + str(angle) + "\n"

            # Send command string with new angle to Arduino
            SerialIOArduino.write(servoString1)
            
    elif direction == 'RIGHT':
        if angle > minAngle:
            angle -= 5 
            servoString2 = "$SERVO1," + str(angle) + "\n"
            SerialIOArduino.write(servoString2)

    # Return new angle
    return angle
''' End servo1Direction '''


''' Method for controlling servo motor 2 '''
def servo2Direction(direction, angle):

    # Maximum and minimum angles
    maxAngle = 175
    minAngle = 5
    
    if direction == 'LEFT':

        # Change servo angle
        if angle < maxAngle:
            angle += 5
            servoString1 = "$SERVO2," + str(angle) + "\n"
            SerialIOArduino.write(servoString1)
            
    elif direction == 'RIGHT':
        if angle > minAngle:
            angle -= 5 
            servoString2 = "$SERVO2," + str(angle) + "\n"
            SerialIOArduino.write(servoString2) 
    
    return angle
''' End servo2Direction '''


''' Method for decoding and executing temperature commands '''
def tempCommands(command = [], *args):
	
    global frequency
    global temperatureThreadRunning
	
    if command[1] == 'REQUEST':

	# Start thread for sending temperatures if it's not already running
        if temperatureThreadRunning == 0:
            temperature_thread.start() 
            temperatureThreadRunning = 1

            # Request temperature from Arduino
            SerialIOArduino.write("$TEMP\n") 

    # Change the frequency of continuous temperature sampling
    elif command[1] == 'SET':
        frequency = int(command[2])
''' End tempCommands '''


''' Thread class for continuous temperature sampling '''
class readout(Thread):

    # Initiate thread
    def __init__(self):
        Thread.__init__(self)

        # Stop thread when program shuts down
        self.daemon = True
        self.start()
        
    # Run thread
    def run(self):
        global frequency
        while (frequency != 0):
            # Request temperature from Arduino
            SerialIOArduino.write("$TEMP\n") 
            time.sleep(frequency)
''' End readout '''
            
# Thread for sending temperatures to Windows computer when they are available
temperature_thread = Thread(target = sendTemperature)
temperature_thread.daemon = True

# Web server thread
web_server = Thread(target = runWebServer)
web_server.daemon = True


''' Method to decode commands sent from Windows computer '''
def receiveUdp(sock):

    # Default angle of servo motors
    angle1 = 90 
    angle2 = 90

    # Reference to IP address of Windows computer 
    global UDP_IP
    
    # Reference variables used so threads won't be started multiple times
    global webServerRunning
    global temperatureThreadRunning
    global frequency
        
    while True:

        # Max recieve size is 1280 bytes
        data, addr = sock.recvfrom(1280) 

        # Split the csv string
        command_string = data.split(",")

        #  End point IP changed to that of Windows computer
        UDP_IP = str(addr[0]) 

                            
        # Decode command strings
        if command_string[0] == '$SERVO1':

            # Change angle of servo motor 1
            angle1 = servo1Direction(command_string[1], angle1)

        elif command_string[0] == '$SERVO2':

            # Change angle of servo motor 2
            angle2 = servo2Direction(command_string[1], angle2)

        # Pass command string to tempCommands
        elif command_string[0] == '$TEMP':
            tempCommands(command_string)

        # Start the web server thread if it's not already running
        elif command_string[0] == '$WEB':
            if webServerRunning == 0:
                web_server.start() 
                webServerRunning = 1

        # Start thread for sending temperatures if it's not already running
        elif command_string[0] == '$BEGINREAD':
            if temperatureThreadRunning == 0:
                temperature_thread.start() 
                temperatureThreadRunning = 1

            # Create object of readout thread for continuous temperature sampling
            TempThread = readout()
            
''' End receiveUdp '''
        
# Thread for receiving UDP messages
udp_listen = Thread(target = receiveUdp, args=(sock,))
udp_listen.daemon = True
udp_listen.start()

print "EN-SOC3001 program is now running"

# Infinite loop to prevent the program from shutting down after the first run-through
while True:
    continue
