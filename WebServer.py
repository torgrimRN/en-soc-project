from SimpleCV import *
from flask import Flask,send_file,request
app = Flask(__name__)

cam = Camera()

@app.route('/webcam')
def SendImage():
    img = cam.getImage()
    img.save("myImg.jpg")
    return send_file('myImg.jpg', mimetype='image/jpg')

if __name__ == '__main__':
    app.run(host='0.0.0.0') #everyone is allowed to access my Server




